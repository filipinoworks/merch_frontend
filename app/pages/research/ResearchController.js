app.controller('ResearchController', function($scope,$stateParams, $window, TokenService, $timeout, $api,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated() && !BYPASS) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

	$scope.template = {};
	$scope.data = {}
	$scope.research_data = {}
	$scope.core = {}

	$scope.getResearchListsNew = function() {
		var url = "/research/new";
		$scope.core.research_loading = true;
        $api.get(url).then(function(response) {
            if(response.status == 200) {
                $scope.data.research_new = response.data
                $scope.core.research_loading = false;
            }
        });
	}

	$scope.getResearchListsFinished = function() {
		var url = "/research/finished";
		$scope.core.research_loading = true;
        $api.get(url).then(function(response) {
            if(response.status == 200) {
                $scope.data.research_finished = response.data
                $scope.core.research_loading = false;
            }
        });
	}

	$scope.visitLink = function(link) {
		if(link.indexOf("http://") < 0 && link.indexOf("https://") < 0){
		   	$window.open("http://"+link, '_blank');
		}else{
			$window.open(link, '_blank');
		}
	}

	$scope.edit_page = function(to, id) {
		var data = {research_id : id}
		$state.go(to, data)
	}

	$scope.saveResearch = function() {
		var url = "/research";
        var data = $scope.research_data;
        data.path_code = $scope.template.path_code != undefined ? $scope.template.path_code : "" 
        $api.post(data, url).then(function(response) {
            $scope.activeButton.stop();
            if(response.status == 200) {
            	$state.go("research")
                $.notify(response.data, "success");
            }
        });
	}

	$scope.make_random = function() {
		var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
	}

	$scope.path_code_make = function() {
		if($scope.template.path_code == null || $scope.template.path_code == "" || $scope.template.path_code == undefined) 
		{
			$scope.template.path_code = $scope.make_random()
		}
	}

	$scope.removePhoto = function(file_id , button) {
		swal({
            title: "Confirm Deletion",
            text: "Are you sure you want to remove this photo?.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Remove",
            closeOnConfirm: true
        },
        function(isConfirm){   
        	if(isConfirm){
        		var url = "/listing/remove/images/"+file_id;
		        $api.delete(url).then(function(response) {
		            if(response.status == 200) {
		            	$(button).parents(".dz-image-preview").remove()
		            	$.notify(response.data, "success");
		            }
		        });
        	}         
            
        });
	}
	$scope.updateStatus = function(status) {
		var url = "/research/update-status";
        var data = {status : status, id : $scope.research_data.id}
        
        $api.post(data, url).then(function(response) {
            if(response.status == 200) {
            	$state.go("research")
                $.notify(response.data, "success");
            }
        });
	}

	$scope.createListing = function() {
		swal({
            title: "Confirm Creation",
            text: "Are you sure you want to create a listing for this research idea?.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#20BF55",
            confirmButtonText: "Create",
            closeOnConfirm: true
        },
        function(isConfirm){   
        	if(isConfirm){
        		var url = "/research/"+$scope.research_data.id+"/create-listing";
		        $api.get(url).then(function(response) {
		            if(response.status == 200) {
		                $.notify(response.data, "success");
		            }
		        });
        	}         
        });
	}

	$scope.removeResearch = function() {
		swal({
            title: "Confirm Deletion",
            text: "Are you sure you want to remove this research idea?.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Remove",
            closeOnConfirm: true
        },
        function(isConfirm){   
        	if(isConfirm){
        		var url = "/research/"+$scope.research_data.id+"/remove";
		        $api.delete(url).then(function(response) {
		            if(response.status == 200) {
		            	$.notify(response.data, "success");
		            	$state.go("research")
		            }
		        });
        	}         
            
        });
	}

	$scope.getResearchById = function() {
		var url = "/research/"+$scope.research_data.id+"/retrieve";
		$scope.core.add_research_loading = true;
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.template.path_code = response.data.research.path_code
                $scope.research_data = response.data.research
                $scope.research_uploads = response.data.photos

                $(".dz-image-preview").html("")
				angular.forEach($scope.research_uploads, function(photo) {
					var mockFile = { id :  photo.id,name: photo.filename, size: "",uploaded : true, width: photo.width, height: photo.height };
					$scope.core.dropzone_photos.emit("addedfile", mockFile);
					$scope.core.dropzone_photos.emit("thumbnail", mockFile, API_URL+"/listing/images/"+photo.id+"?token="+localStorageService.get("token"));
				});

				$scope.core.add_research_loading = false
            }
        });
	}

	$scope.init = function() {
		$scope.path_code_make();
		$scope.getResearchListsNew();
		$scope.getResearchListsFinished();
	}

	$scope.init();

	if($stateParams.research_id != undefined && $stateParams.research_id != "") {
		$scope.research_data.id = $stateParams.research_id;
		$scope.getResearchById();
	}


	
});

