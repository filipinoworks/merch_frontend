app.controller('SalesChannelController', function($scope, $window,$stateParams, TokenService, $timeout, $api,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated() && !BYPASS) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}


	$scope.data = {};
	$scope.picker = {}
	$scope.sales_channel = {};
	$scope.sales_product = {};
	$scope.sales_product.light_colors = [];
	$scope.sales_product.dark_colors = [];
	$scope.data.sales_channel_products = [];

	$scope.saveSalesChannel = function() {
		var url = "/sales_channel";
        var data = $scope.sales_channel;
        
        $api.post(data, url).then(function(response) {
            $scope.activeButton.stop();
            if(response.status == 200) {
            	$state.go("sales_channel")
                $.notify(response.data, "success");
            }
        });
	}

	$scope.getSalesChannelById = function(sales_channel_id) {
		var url = "/sales_channel/"+sales_channel_id+"/edit"
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.sales_channel = response.data
            	console.log($scope.sales_channel)
            }
        });
	}

	$scope.updatePickerModel = function(color) {
		if(typeof $scope.product[$scope.current_picker.key] == "string") {
			$scope.product[$scope.current_picker.key] = JSON.parse($scope.product[$scope.current_picker.key]);
		}
		$scope.product[$scope.current_picker.key][$scope.current_picker.index] = color
		$scope.current_picker = {}
	}

	$scope.initPicker = function(key, index, picker_id) {
		$scope.current_picker = {};
		$scope.current_picker.key = key;
		$scope.current_picker.index = index;
		$scope.current_picker.picker_id = picker_id
	}


	$scope.convertStringToArray = function(string){
		if(string == undefined) {
			return [];
		}
		return  JSON.parse(string) == null ? [] : JSON.parse(string)
	}

	$scope.getSalesChannel = function(){
		var url = "/sales_channel";
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.data.sales_channel = response.data
            }
        });
	}


	$scope.getSalesChannelProducts = function(){
		var url = "/sales_channel/product?sales_channel_id="+$stateParams.sales_channel_id;
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	console.log(response.data)
            	$scope.data.sales_channel_products = response.data
            	angular.forEach($scope.data.sales_channel_products, function(sales_channel_products) {

		            sales_channel_products.light_colors = JSON.parse(sales_channel_products.light_colors) == null ? [] : JSON.parse(sales_channel_products.light_colors)
		            sales_channel_products.dark_colors = JSON.parse(sales_channel_products.dark_colors) == null ? [] : JSON.parse(sales_channel_products.dark_colors)
		        });
            	console.log($scope.data.sales_channel_products)
            	
            }
        });
	}
	$scope.getProductsNotInSalesChannel = function(){
		var url = "/products?not_in_sales_channel=true&sales_channel_id="+$stateParams.sales_channel_id;
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.data.products = response.data
            }
        });
	}

	$scope.saveSalesChannelProduct = function() {
		
		var url = "/sales_channel/product";
        var data = $scope.product

        console.log(data)
        $api.post(data, url).then(function(response) {
            $scope.activeButton.stop();
            if(response.status == 200) {
            	$state.go("sales_channel")          
                $.notify(response.data, "success");
            }
        });


	}

	$scope.edit_page = function(to, id){
		$state.go(to, {sales_channel_id: id})
	}

	$scope.editSalesProduct = function(sales_product) {
		$state.go("edit_sales_product", {sales_channel_product_id: sales_product.id})
	}

	$scope.getNum = function(num, key) {

		if($scope.product == undefined) {
			return new Array(num);   
		}

		if(typeof $scope.product[key] == "string"){
			$scope.product[key] = JSON.parse($scope.product[key]);
		}



		if($scope.product[key] != undefined) {
			if($scope.product[key].length > 0) {
				if($scope.product[key].length < num) {
					var extra_array = new Array(num - $scope.product[key].length);  
					return $scope.product[key].concat(extra_array);
				}
				return $scope.product[key];
			}
		}
		

		return new Array(num);   
	}

	$scope.addNewDarkColor = function(src_model) {
		src_model.push("#000")
		$timeout(function() {
			$(".colorpick").colorpicker();
		},200)
	}

	$scope.removeDarkColor = function($index) {
		$scope.sales_product.dark_colors.splice($index, 1)

		$timeout(function() {
			$(".colorpick").colorpicker();
		},200)
	}

	$scope.addNewLightColor = function(src_model) {
		src_model.push("#fff")
		$timeout(function() {
			$(".colorpick").colorpicker();
		},200)
	}

	$scope.removeLightColor = function($index) {
		$scope.sales_product.light_colors.splice($index, 1)

		$timeout(function() {
			$(".colorpick").colorpicker();
		},200)
	}

	$scope.removeProductFromSalesChannel = function() {

		swal({
            title: "Confirm Deletion",
            text: "This product will be removed from the system.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Remove",
            closeOnConfirm: true
        },
        function(isConfirm){            
            var url = "/sales_channel/product/"+$scope.sales_product.id+"/remove";
	        $api.delete(url).then(function(response) {
	            if(response.status == 200) {
	            	$scope.getSalesChannelProducts();
					$scope.getProductsNotInSalesChannel();
					$("#edit_product_sales_channel").modal("hide")
	            }
	        });
        });


	}

	$scope.resetSalesProduct = function(){
		$scope.sales_product = {};
		$scope.sales_product.light_colors = [];
		$scope.sales_product.dark_colors = [];
		$scope.sales_product.sales_channel_id = $stateParams.sales_channel_id
	}


	$scope.addToSalesChannel = function(product, src_index) {

		$scope.data.products.splice(src_index, 1)

		var data = {};
		data.light_colors = ["#fff"];
		data.dark_colors = ["#000"];
		data.max_colors = 0;
		data.price = 0;
		data.sales_channel_id = $stateParams.sales_channel_id;
		data.product_id = product.id;
		data.product_name = product.product_name;

		var url = "/sales_channel/product";
        
        $api.post(data, url).then(function(response) {
            
            if(response.status == 200) {
            	$scope.getSalesChannelProducts();
                $.notify(response.data, "success");
            }
        });

		
	}

	$scope.getSalesChannelProductById = function() {
		var url = "/sales_channel/edit/"+$scope.sales_channel_product_id+"/product";
        
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.product = response.data
            	console.log($scope.product)
            	setTimeout(function() {
            		$scope.initPicker2();
            	}, 400);
            	
            }
        });
	}


	$scope.initPicker2 = function() {
	
		angular.forEach(angular.element(".color-picker"), function(picker, key){
			var product = $scope.product
	        var default_colors = $scope.convertStringToArray(product.default_colors)
	        var current_color = $(picker).attr("data-color")
	        console.log(default_colors)
	        $(picker).spectrum({
	            color: current_color,
	            showPaletteOnly: true,
	            showPalette: true,
	            palette: [ ], 
	            maxSelectionSize: 21,
	            showSelectionPalette: true, // true by default
	            selectionPalette: default_colors
	        });
		});


	}


	if($state.current.name == "sales_channel") {
		$scope.getSalesChannel();
	}

	if($state.current.name == "edit_sales_channel" && $stateParams.sales_channel_id != undefined) {
			$scope.getSalesChannelById($stateParams.sales_channel_id);
			$scope.getSalesChannelProducts();
	}

	if($stateParams.sales_channel_product_id != undefined && $stateParams.sales_channel_product_id != "") {
		$scope.sales_channel_product_id = $stateParams.sales_channel_product_id;
		$scope.getSalesChannelProductById();
	}

});

