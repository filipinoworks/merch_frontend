app.controller('SigninController', function($scope, $window, $http,TokenService, $timeout, $api,localStorageService, $state, AunthenticationService) {
	
	if(AunthenticationService.isAuthenticated() && !BYPASS) {
		$window.location.href = '/#/dashboard';
	}

    $(".loading-base").removeClass("hidden");
	$scope.token;
    $scope.loginData = {};
    $scope.loginSuccess = "";
    
    if($.cookie('remember') == 'true'){
        $scope.rememberme = true;
        $scope.loginData.email = $.cookie('email');
        $scope.loginData.password = $.cookie('password');
    }else{
        $scope.loginData.email = "";
        $scope.loginData.password = "";
    }
    $scope.close_previous_slider = function(){      
        $('#close-slider-form').trigger('click');
        return false;
    }
    $scope.isAuthenticated = function() {
        return false;
    }
    $scope.sendConfirmationEmail = function() {
        var url = "/emails/sendConfirmationEmail";
        var data = $scope.loginData;
        
        $api.post(data, url).then(function(response) {
            if(response.data.status == true) {
                //
                $.notify("Email confirmation was sent. Check ur email.", "success");
            }
        });
    }
    $scope.resendPassword = function() {    
        var is_local = environment  
        //is_local = "local"
        if(is_local != "local" && (grecaptcha.getResponse() == null || grecaptcha.getResponse() == "")) {
            $scope.errorMessage = "Captcha is invalid. ";
            $scope.$apply();
            $timeout(function() {
                $scope.errorMessage = "";
            }, 3000);
            $scope.activeButton.stop();

            return;
        }else{

            var url = "/emails/sendPasswordForgot";
            var data = $scope.resendPasswordData;
            
            $api.post(data, url).then(function(response) {
                $scope.activeButton.stop();
                if(response.data.status == true) {
                    $scope.successMessage = response.data.message;
                    $timeout(function() {
                        $scope.successMessage = "";
                        $window.location.href = '/authentication/#/signin';
                    }, 3000);
                }else{

                    $scope.errorMessage = response.data.message;

                    $timeout(function() {
                        $scope.errorMessage = "";
                    }, 3000);
                    
                }
            });
        }
    }

    
    $scope.submitLogin = function() {
        
        angular.forEach($scope.loginForm.$error.required, function(field) {
            field.$dirty = true;
            field.$setPristine();
        });

        if($scope.loginForm.$valid) {

            AunthenticationService.login($scope.loginData).then(function(response) {
        
                if(response.status == 200) {

                    if($scope.rememberme ==true){
                        $.cookie('email', $scope.loginData.email, { expires: 14 });
                        $.cookie('password', $scope.loginData.password, { expires: 14 });
                        $.cookie('remember', true, { expires: 14 });                            
                    }else{
                        $.cookie('email', null, {});
                        $.cookie('password', null, {});
                        $.cookie('remember', null, {});
                    }
                    $scope.activeButton.stop();
                    $scope.loginData.password = "";
                    $scope.loginForm.$setPristine();
                    AunthenticationService.setAuth(true);
                    $scope.$parent.authenticated = true;
                    AunthenticationService.setData(response.data.credentials);
                    localStorageService.remove("is_lock");  
                    localStorageService.set("credentials", response.data.credentials);
                    localStorageService.set("profile", response.data.profile);
                    localStorageService.set("token", response.data.token);
                    localStorageService.set("role", response.data.role);
                    $http.defaults.headers.common.Authorization = 'Bearer  '+ localStorageService.get("token");
                    $scope.loginSuccess  = true;
                    $scope.confirmedEmail = true;

                    $(".loading-base").removeClass("hidden");

                    $timeout(function() {
                        $window.location.href = '/#/dashboard';
                    },1500);
                }
                else if(response.data.status == "unconfirmed") {
                    $scope.activeButton.stop();
                    $scope.loginData.password = "";
                    $scope.loginSuccess  = false;
                    $scope.confirmedEmail = false;
                    $('.modal-content').css({
                        height : '570px',                   
                    })
                }else{
                    if(response.status == 401) {
                        $.notify("Wrong Credentials, please try again.", "warning");
                        $scope.activeButton.stop();
                        $scope.loginData.password = "";
                        $scope.loginSuccess  = false;
                        $scope.loginError = response.data.message
                    }
                    
                }
            });
        }
    }   


    $scope.register = function() {

        AunthenticationService.register($scope.loginData).then(function(response) {
            if(response.status == 200) {              
                AunthenticationService.setAuth(false);
                AunthenticationService.setData({});
                localStorageService.remove("credentials");
                localStorageService.remove("profile");
                $scope.authenticated = false;//parent controller

                swal("Registration: ", "You have been registered. Redirecting..", "success");
                $scope.activeButton.stop();
                $scope.loginData = {};
                $scope.loginForm.$setPristine();

                AunthenticationService.setAuth(true);
                AunthenticationService.setData(response.data.credentials);
                localStorageService.set("credentials", response.data.credentials);
                localStorageService.set("profile", response.data.profile);
                localStorageService.set("token", response.data.token);
                localStorageService.set("role", response.data.role);
                $http.defaults.headers.common.Authorization = 'Bearer  '+ localStorageService.get("token");
                console.log(response.data);

                $timeout(function() {
                    $window.location.href = '/#/signin';
                }, 1500);
                
            }else if(response.data.status == false){
                swal("Registration: ", response.data.message, "warning");
                
            }else{
                $scope.activeButton.stop();
            }
        });
    }

    


    $scope.reload_custom = function() {
        $('<script>').attr('src', "https://www.google.com/recaptcha/api.js").appendTo('head');
    }


    $scope.reload_custom();

    $timeout(function() {
        $(".loading-base").addClass("hidden");
    },1000)
});

