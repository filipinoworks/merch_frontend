app.controller('PlansController', function($scope,$stateParams, $window, TokenService, $timeout, $api,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated() && !BYPASS) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

	$scope.steps = {}
    $scope.cardinfo = {}
    $scope.selected_plan = {};
	$scope.card_id = $stateParams.card_id;
	$scope.check_id = $stateParams.check_id;
	$scope.initialization = false;
	$scope.billData = {};
	$scope.checkInfo = {};
	$scope.cards = {};
	$scope.checks = {};
    $scope.billData.cardNumber = "";
    $scope.billData.firstName = "";
    $scope.billData.lastName = "";
    $scope.billData.expMonth = "";
    $scope.billData.expYear = "";
    $scope.billData.cardIDNumber = "";
    $scope.billData.address = "";
	$scope.title = 'Add a billing method';

    $scope.is_blank = function(src) {
        if(src == undefined || src == '') {
            return true
        }

        return false
    }

    $scope.address_checker = function() {
        if($scope.is_blank($scope.cardinfo.name)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.card_number)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.exp_month)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.exp_year)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.cvc)) {
            return false
        }

        return true
    }

    $scope.basic_info_checker = function() {
       
        if($scope.is_blank($scope.cardinfo.name)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.card_number)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.exp_month)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.exp_year)) {
            return false
        }

        if($scope.is_blank($scope.cardinfo.cvc)) {
            return false
        }

        return true
    }


    $scope.steps.current = "basic_info"
    $scope.steps.rules =  {
        "basic_info" : $scope.basic_info_checker()
    }



    $scope.getExpYears = function() { 
        var year = 2018;
        var array_year = [];
        for(year; year <= 2035; year = year + 1) {
            array_year.push(year);
        }

        return array_year;
    }
	
	$scope.expYears = $scope.getExpYears()


	$scope.getPlans = function() {
		var url = "/plans"
		$api.get(url).then(function(response) {
            if(response.status == 200) {
               $scope.plans = response.data
            }
        });
	}

	$scope.savePlan = function() {
		var data = {};
		var url = "/plans/select-plan"
		data.plan = $scope.selected_plan
		data.card = $scope.cardinfo;
		console.log(data);
		$api.post(data, url).then(function(response) {
			$scope.activeButton.stop();
            if(response.status == 200) {
               console.log("fine")
            }
        });

	}

    $scope.stepsGoto = function(step) {
        $scope.steps.current = step
    }


	$scope.subscribe = function(plan) {
		$("#card-modal").modal("show")

		$scope.selected_plan = plan
	}

	$scope.getPlans();
});