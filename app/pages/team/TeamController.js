app.controller('TeamController', function($scope,$stateParams, $window, TokenService, $timeout, $api,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated() && !BYPASS) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}

	$scope.data = {};
	$scope.team_data = {};

	$scope.getTeamMembers = function() {
		var url = "/team-members";
		$scope.core.team_loading = true;
		$api.get(url).then(function(response) {
            if(response.status == 200) {
				$scope.data.team_members = response.data                
            }

            $scope.core.team_loading = false;
        });
	}

	$scope.saveTeam = function() {
		var url = "/team";
        var data = $scope.team_data;

        if($scope.team_data.password != $scope.team_data.confirm_password) {
        	$.notify("Password and Confirm password must match.", "warning");
        	$scope.activeButton.stop();
        	return;
        }

        if($scope.team_data.role == "") {
        	$.notify("Role is required.", "warning");
        	$scope.activeButton.stop();
        	return;
        }

        $api.post(data, url).then(function(response) {
            $scope.activeButton.stop();
            if(response.status == 200) {
            	$state.go("team")
                $.notify(response.data.message, "success");
            }
        });
	}

	$scope.edit_page = function(to, id) {
		var data = {user_id : id}
		$state.go(to, data)
	}

	$scope.displayRoles = function(roles) {
		return roles.join(",");
	}

	$scope.editMode = function() {
		if($scope.team_data.id != undefined && $scope.team_data.id != "") {
			return true;
		}

		return false;
	}

	$scope.getUserById = function() {
		var url = "/team/user/"+$scope.team_data.id+"/retrieve";
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.core.team_loading = false;
            	$scope.team_data.id = response.data.id
            	$scope.team_data.firstName = response.data.user_profile.firstName
            	$scope.team_data.lastName = response.data.user_profile.lastName
            	$scope.team_data.email = response.data.email
            	$scope.team_data.role = response.data.role
            }

            if(response.status == 403) {
            	$.notify("Sorry you are not authorized to view the user details.", "success");
            	$scope.core.team_loading = false;
            	$state.go("team")
            }


        });
	}

	

	if($stateParams.user_id != undefined && $stateParams.user_id != "") {
		$scope.team_data.id = $stateParams.user_id;
		$scope.core.team_loading = true;
		$timeout(function() {
			$scope.getUserById();
		}, 500)
		
	}else{
		$scope.getTeamMembers();
	}

});