app.controller('ListingController', function($scope, $interpolate , $rootScope, $window, TokenService, $timeout, $api,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated() && !BYPASS) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}
	$scope.key_settings = {}
	$scope.core = {};
	$scope.core.loading = true;
	$scope.core.listing_loading = true;
	$scope.data = {};
	$scope.data.edit_mode = false;
	$scope.vim = {};
	$scope.template = {}
	$scope.template.is_bind = true;

	$scope.parseContent = function(template) {
	    return $interpolate(template)($scope);
	};

	$scope.keywordsReplacement = function(oldString) {
		var range = 15;
		if(oldString == null) {
			return oldString;
		}
		for (var i = range - 1; i >= 0; i--) {
			var old_val = "%"+i
			if($scope["key"+i] == undefined || $scope["key"+i] == "" || $scope["key"+i] == "%"+i){
				$scope["key"+i] = old_val;
			}
			oldString = oldString.replace("%"+i,"{{key"+i+"}}");
		}
		return oldString;
	}

	$scope.convertStringToArray = function(string){
		if(string == undefined) {
			return [];
		}
		if(typeof string == "string") {
			return  JSON.parse(string)
		}

		return  string
	}

	$scope.displayExactData = function(product , key) {

		if(product == null) {
			return;
		}
		if($scope.template.is_bind == true) {
			return $scope.template[key] != undefined && $scope.template[key] != "" ? $scope.template[key] : $scope.stringReplacements(product[key], product) ;
		}else{
			return $scope.stringReplacements(product[key], product);
		}
	}

	$scope.updateData = function(product , data, key) {
		product[key] = data;
		$scope.stringReplacements(data, product);
	}

	$scope.stringReplacements = function(oldString, product, key) {
	
		if(oldString == null) {return null}

		oldString = $scope.keywordsReplacement(oldString);

		if($scope.template.bp_one != "" && $scope.template.bp_one != undefined) {
			if($scope.template.is_bind == true) {
				oldString = oldString.replace("%bp1","{{template.bp_one}}");
			}
		}else{
			if(product.bp_one == undefined || product.bp_one == "") {
				//product.bp_one = "%bp1"
			}else{
				product.bp_one = $scope.keywordsReplacement(product.bp_one);
				oldString = oldString.replace("%bp1",product.bp_one);
			}
		}

		if($scope.template.bp_two != "" && $scope.template.bp_two != undefined) {
			oldString = oldString.replace("%bp2","{{template.bp_two}}");
		}else{
			if(product.bp_two == undefined || product.bp_two == "") {
				//product.bp_two = "%bp2"
			}else{
				product.bp_two = $scope.keywordsReplacement(product.bp_two);
				oldString = oldString.replace("%bp2",product.bp_two);
			}
		}

		if($scope.template.is_bind == true) {
			oldString = oldString.replace("%title","{{template.title}}");
			oldString = oldString.replace("%brand","{{template.brand}}");
		}
		

		return $scope.parseContent(oldString);
	}

	$scope.removePhotoInListing = function(listing_id, phone) {
		var list = {}
		angular.forEach($scope.data.lists, function(list_src) {
			if(list_src.id == listing_id) {
				list = list_src
			}
		});
		return list;
	}


	$scope.removePhoto = function(file_id , button) {
		swal({
            title: "Confirm Deletion",
            text: "Are you sure you want to remove this photo?.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Remove",
            closeOnConfirm: true
        },
        function(isConfirm){   
        	if(isConfirm){
        		var url = "/listing/remove/images/"+file_id;
		        $api.delete(url).then(function(response) {
		            if(response.status == 200) {
		            	$(button).parents(".dz-image-preview").remove()
		            	$.notify(response.data, "success");
		            }
		        });
        	}         
            
        });
	}
	$scope.getCurrentListing = function() {
		var list = {};
		angular.forEach($scope.data.lists, function(list_src) {
			if(list_src.id == $scope.template.id) {
				list = list_src
			}
		});

		return list
	}

	$scope.removeListing = function() {

		swal({
            title: "Confirm Deletion",
            text: "This listing will be removed from the system.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Remove",
            closeOnConfirm: true
        },
        function(isConfirm){            
            var url = "/listing/remove/"+$scope.template.id;
	        $api.delete(url).then(function(response) {
	            if(response.status == 200) {
	            	$scope.data.edit_mode = false
	            	$scope.getLists();
	            	$.notify(response.data, "success");
	            }
	        });
        });


		
	}

	$scope.getSalesChannel = function(){
		var url = "/sales_channel";
		$scope.core.loading = true
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.data.sales_channel = response.data

            	angular.forEach($scope.data.sales_channel, function(channel) {
            		angular.forEach(channel.products, function(product) {
	            		if(typeof product.light_colors == "string"){
							product.light_colors = JSON.parse(product.light_colors);
						}

						if(typeof product.dark_colors == "string"){
							product.dark_colors = JSON.parse(product.dark_colors);
						}
	            	});
            	});

            }

            $scope.core.loading = false;
        });
	}

	$scope.bindUnbind = function() {
		var bind = $scope.template.is_bind = !$scope.template.is_bind;
		console.log(bind)
		return bind;
	}

	$scope.undefinedFixer = function(object, key) {
		return object[key] != undefined ? object[key] : ""; 
 	}

	$scope.prepareListingData = function() {
		var data = angular.copy($scope.data.sales_channel)
		var results = [];

		var container              = {};

		
		
		container.id                    = $scope.template.id 
		container.input_designer        = $scope.template.input_designer 
		container.unregistered_designer = $scope.template.unregistered_designer 
		container.path_code             = $scope.template.path_code 
		container.designer              = $scope.template.designer   
		container.title                 = $scope.undefinedFixer($scope.template, "title");
		container.brand                 = $scope.undefinedFixer($scope.template, "brand");
		container.keywords              = $scope.undefinedFixer($scope.template, "keywords");
		container.bp_one                = $scope.undefinedFixer($scope.template, "bp_one");
		container.bp_two                = $scope.undefinedFixer($scope.template, "bp_two");
		container.is_bind               = $scope.undefinedFixer($scope.template, "is_bind");
		container.description           = $scope.undefinedFixer($scope.template, "description");
		

		angular.forEach(data, function(channel) {
			

			if(container.products == undefined) {
				container.products = []
			}

            angular.forEach(channel.products, function(product) {
            	
            	var temp_product = {}

            	if(product.listing_id != undefined && product.listing_id != "") {
            		temp_product.id = product.id
            	}

				temp_product.src_product_id   = product.id
				temp_product.price            = product.price
				temp_product.sales_channel_id = product.sales_channel_id
				temp_product.light_colors     = product.light_colors
				temp_product.dark_colors      = product.dark_colors
				temp_product.product_name     = product.product_name
				temp_product.is_light         = product.is_light
				temp_product.selected_color_profile   = product.selected_color_profile
				temp_product.selected_colors  = product.selected_colors
				temp_product.link             = product.link
				temp_product.default_colors   = product.default_colors
				temp_product.title            = $scope.displayExactData(product, "title")
				temp_product.brand            = $scope.displayExactData(product, "brand")
				temp_product.bp_one           = $scope.displayExactData(product, "bp_one")
				temp_product.bp_two           = $scope.displayExactData(product, "bp_two")
				temp_product.description      = $scope.displayExactData(product, "description")
				
            	container.products.push(temp_product)

            });

        });

        return container


	}

	$scope.removeSelectedColor = function(product, index) {
		product.selected_colors.splice(index, 1)
		$timeout(function() {
			$scope.colorPicker();
		}, 300)
	}

	$scope.addSelectedColor = function(product) {
		product.selected_colors.push("#000");
		$timeout(function() {
			$scope.colorPicker();
		}, 300)
	}

	$scope.getLists = function() {
		var url = "/listing";
		$scope.core.listing_loading = true
        $api.get(url).then(function(response) {
            if(response.status == 200) {
                $scope.data.lists = response.data
                
                angular.forEach($scope.data.lists, function(list) {
                	list.status = $scope.getStatus(list)
                });
            }
            $scope.core.listing_loading = false
        });
	}

	$scope.saveListing = function() {
		var url = "/listing";
        var data = $scope.prepareListingData();
        
        $api.post(data, url).then(function(response) {
            $scope.activeButton.stop();
            if(response.status == 200) {
            	$scope.getLists();
            	$scope.data.edit_mode = false
                $.notify(response.data, "success");
            }
        });
	}

	

	$scope.make_random = function() {
		var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
	}


	$scope.resetAddNewForm = function() {

		$scope.template = {}
		$scope.template.is_bind = true;
		if($scope.template.path_code == null || $scope.template.path_code == "" || $scope.template.path_code == undefined) 
		{
			$scope.template.path_code = $scope.make_random()
		}
		$scope.getSalesChannel();
	}

	$scope.addNew = function() {
		

		if($scope.data.edit_mode) {
			swal({
	            title: "Confirm Closing",
	            text: "Are you sure you want to close this, you will lose unsave changes.",
	            type: "info",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Close",
	            closeOnConfirm: true
	        },
	        function(isConfirm){   
	        	if(isConfirm){
	        		$scope.data.edit_mode = !$scope.data.edit_mode
	        		$(".dz-image-preview").html("")
					$scope.resetAddNewForm();
					return 
	        	}         
	            
	        });
		}else{
	        $scope.data.edit_mode = !$scope.data.edit_mode
			$(".dz-image-preview").html("")
			$scope.resetAddNewForm();
		}
		


		
	}

	$scope.getThumbnail = function(data) {
		var photos = data.photos;
		var photo = photos.length > 0 ? photos[0] : null;
		if(photo != null) {
			var url = API_URL+"/listing/images/"+photo.id+"?token="+localStorageService.get("token");
			return url;
		}else{
			return null
		}
	}

	$scope.colorPicker = function() {
		$(".input-picker").change();
	}
	$scope.changeSelectedColors = function(key, index, picker_id) {
		$scope.current_picker = {};
		$scope.current_picker.key = key;
		$scope.current_picker.index = index;
		$scope.current_picker.picker_id = picker_id

		
	}

	$scope.resetPicker = function() {
		$scope.current_picker = {};
	}



	$scope.chooseColor = function(product) {
		
		product.selected_colors = {};
		
		if(product.selected_color_profile == "dark_colors") {
			product.selected_colors = product.dark_colors;
		}else{
			product.selected_colors = product.light_colors;
		}
		if(typeof product.selected_colors == "string") {
			product.selected_colors = JSON.parse(product.selected_colors);
		}

		if(product.selected_colors.length == 0) {
			var default_colors = new Array(product.max_colors)
			product.selected_colors = default_colors
		}


		$timeout(function() {
			setTimeout(function() {$scope.initPicker2(product.selected_colors);}, 400);
		}, 300)

	}

	$scope.getColors = function(product) {

		return new Array(num);   
	}

	$scope.updatePickerModel = function(product, color) {
		if(typeof product[$scope.current_picker.key] == "string") {
		 	product[$scope.current_picker.key] = JSON.parse(product[$scope.current_picker.key]);
		}
	 	product[$scope.current_picker.key][$scope.current_picker.index] = color
		$scope.current_picker = {}
	}

	$scope.displayCreator = function(list) {
		if(list.creator != undefined && list.creator != "") {
			return list.creator.user_profile.firstName + " " + list.creator.user_profile.lastName;
		}
		return  "";
	}

	$scope.displayDesigner = function(list) {
		if(list.designer_name != undefined && list.designer_name != "") {
			return list.designer_name.user_profile.firstName + " " + list.designer_name.user_profile.lastName;
		}
		return  "";
	}

	$scope.initPicker2 = function(selected_colors) {
		angular.forEach(angular.element(".color-picker"), function(picker, key){
			var product = $(picker).attr("data-product")
	        var default_colors = selected_colors != undefined ? selected_colors : $scope.convertStringToArray(product.selected_colors)
	        var current_color = $(picker).attr("data-color")

	        console.log(default_colors)

       		$(picker).spectrum({
       			color: current_color,
       			showPaletteOnly: true,
	            showPalette: true,
	            palette: [ ], 
	            showSelectionPalette: true, // true by default
	            palette: [default_colors]
	        });
	        

	       	$(picker).spectrum("set", current_color);


	        $scope.$apply();
		});

		
	}

	$scope.editListing = function(data) {
		$scope.data.sales_channel = {};
		$scope.core.loading = true;
		$scope.data.edit_mode = true;

		var container              = {};
		container.title            = data.title;
		container.input_designer   = data.input_designer; 
		container.unregistered_designer    = data.unregistered_designer; 
		container.brand            = data.brand;
		container.keywords         = data.keywords;
		container.designer         = data.designer;
		container.bp_one           = data.bp_one;
		container.bp_two           = data.bp_two;
		container.is_bind          = data.is_bind
		container.description      = data.description;
		container.path_code        = data.path_code;
		container.notes      	   = data.notes;

		$scope.template = container
		$scope.template.id = data.id

		$scope.data.sales_channel = data["sales_channels"];
		console.log($scope.template)


		setTimeout(function() {$scope.initPicker2();}, 400);

		var url = "/listing/get-images-by-listing/"+$scope.template.id;
        $api.get(url).then(function(response) {
            if(response.status == 200) {
                data.photos = response.data
                $scope.core.loading = false;
				$(".dz-image-preview").html("")
				angular.forEach(data.photos, function(photo) {
					var mockFile = { id :  photo.id,name: photo.filename, size: "",uploaded : true, width: photo.width, height: photo.height };
					$scope.core.dropzone_photos.emit("addedfile", mockFile);
					$scope.core.dropzone_photos.emit("thumbnail", mockFile, API_URL+"/listing/images/"+photo.id+"?token="+localStorageService.get("token"));
				});

				
				

            }
        });
	}

	$scope.initKeyEvents = function() {
		$(document).keyup(function(ev) {
			if(ev.originalEvent.key.toLowerCase() == "w") {
				$scope.key_settings.image_bg = !$scope.key_settings.image_bg;
				console.log($scope.key_settings.image_bg)
				$scope.$apply();
			}
		})
	}

	$scope.getTeamMembers = function() {
		var url = "/team-members";
		$api.get(url).then(function(response) {
            if(response.status == 200) {
				$scope.data.team_members = response.data                
            }
        });
	}

	$window.onclick = function(event) {
	    if (!$scope.current_picker) {
	      $scope.current_picker = {};
	      $scope.$apply();
	    }
	}

	$scope.getStatus = function(listing) {
		var url = "/status/"+listing.id;
		$api.get(url).then(function(response) {
            listing.status = response.data;
        });
	}

	$scope.$on('$stateChangeStart', function($event, next, current) { 
	  	
	  	if($scope.data.edit_mode) {
	  		$event.preventDefault();
			swal({
	            title: "Confirm Closing",
	            text: "Are you sure you want to close this, you will lose unsave changes.",
	            type: "info",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Close",
	            closeOnConfirm: true
	        },
	        function(isConfirm) {   

	        	if(isConfirm){
	        		$scope.data.edit_mode = false
	        		$state.go(next.name);
	        	}         
	            
	        });
		}	

	});

	$scope.initKeyEvents();
	$scope.getSalesChannel();
	$scope.getLists();
	$scope.getTeamMembers();
});

