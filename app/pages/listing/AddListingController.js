app.controller('AddListingController', function($scope, $interpolate , $window, TokenService, $timeout, $api,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated() && !BYPASS) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}


	$scope.data = {};
	$scope.vim = {};
	$scope.template = {}
	$scope.template.bind = true;

	$scope.parseContent = function(template) {
	    return $interpolate(template)($scope);
	};

	$scope.keywordsReplacement = function(oldString) {
		var range = 15;
		if(oldString == null) {
			return oldString;
		}
		for (var i = range - 1; i >= 0; i--) {
			var old_val = "%"+i
			if($scope["key"+i] == undefined || $scope["key"+i] == "" || $scope["key"+i] == "%"+i){
				$scope["key"+i] = old_val;
			}
			oldString = oldString.replace("%"+i,"{{key"+i+"}}");
		}
		return oldString;
	}

	$scope.displayExactData = function(product , key) {

		if($scope.template.bind == true) {
			return $scope.template[key] != undefined && $scope.template[key] != "" ? $scope.template[key] : $scope.stringReplacements(product[key], product) ;
		}else{
			return $scope.stringReplacements(product[key], product);
		}
	}

	$scope.updateData = function(product , data, key) {
		product[key] = data;
		$scope.stringReplacements(data, product);
	}

	$scope.stringReplacements = function(oldString, product, key) {
	
		if(oldString == null) {return null}

		oldString = $scope.keywordsReplacement(oldString);

		if($scope.template.bp_one != "" && $scope.template.bp_one != undefined) {
			if($scope.template.bind == true) {
				oldString = oldString.replace("%bp1","{{template.bp_one}}");
			}
		}else{
			if(product.bp_one == undefined || product.bp_one == "") {
				//product.bp_one = "%bp1"
			}else{
				product.bp_one = $scope.keywordsReplacement(product.bp_one);
				oldString = oldString.replace("%bp1",product.bp_one);
			}
		}

		if($scope.template.bp_two != "" && $scope.template.bp_two != undefined) {
			oldString = oldString.replace("%bp2","{{template.bp_two}}");
		}else{
			if(product.bp_two == undefined || product.bp_two == "") {
				//product.bp_two = "%bp2"
			}else{
				product.bp_two = $scope.keywordsReplacement(product.bp_two);
				oldString = oldString.replace("%bp2",product.bp_two);
			}
		}

		if($scope.template.bind == true) {
			oldString = oldString.replace("%title","{{template.title}}");
			oldString = oldString.replace("%brand","{{template.brand}}");
		}
		

		return $scope.parseContent(oldString);
	}

	$scope.getSalesChannel = function(){
		var url = "/sales_channel";
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.data.sales_channel = response.data
            }
        });
	}

	$scope.bindUnbind = function() {
		var bind = $scope.template.bind = !$scope.template.bind;
		console.log(bind)
		return bind;
	}

	$scope.getSalesChannel();
});

