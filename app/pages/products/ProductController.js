app.controller('ProductController', function($scope,$stateParams, $window, TokenService, $timeout, $api,localStorageService, $state, AunthenticationService) {
	if(!AunthenticationService.isAuthenticated() && !BYPASS) {
		if(localStorageService.get("credentials") != null) {
			$scope.$parent.authenticated = true;
			AunthenticationService.setAuth(true);
			AunthenticationService.setData(localStorageService.get("credentials"));
		}else{
			$scope.$parent.authenticated = false;
			AunthenticationService.setAuth(false);
			$window.location.href = '/';
		}
	}


	$scope.data = {};
	$scope.product_data = {};

	$scope.getProducts = function(){
		var url = "/products";
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.data.products = response.data
            }
        });
	}


	$scope.saveProduct = function(){
		var url = "/products";
        var data = $scope.product_data;
        
        $api.post(data, url).then(function(response) {
            $scope.activeButton.stop();
            if(response.status == 200) {
            	$state.go("products")
                $.notify("New Product has been saved!", "success");
            }
        });
	}	

	$scope.getProductById = function(product_id) {
		var url = "/products/"+product_id;
        $api.get(url).then(function(response) {
            if(response.status == 200) {
            	$scope.product_data = response.data
            }
        });
	}

	$scope.removeProduct = function(product_id) {

		swal({
            title: "Confirm Deletion",
            text: "This product will be removed from the system.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Remove",
            closeOnConfirm: true
        },
        function(isConfirm){            
            var url = "/products/"+product_id;
	        $api.delete(url).then(function(response) {
	            if(response.status == 200) {
	            	$.notify("Product has been removed.", "success");
	            	$state.go("products")
	            }
	        });
        });

		
	}


	$scope.edit_page = function(to, id){
		$state.go(to, {product_id: id})
	}

	$timeout(function() {
		if($state.current.name == "products") {
			$scope.getProducts();
		}
		if($state.current.name == "edit_product" && $stateParams.product_id != undefined) {
			$scope.getProductById($stateParams.product_id);
		}

	}, 500)
		
	
});

