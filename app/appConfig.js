app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $urlRouterProvider.otherwise('/signin');
   // $locationProvider.html5Mode(true);

    $stateProvider


        .state('signin', {
            url: '/signin',
            title: 'Signin',
            templateUrl: 'app/pages/authentication/signin.html'+APP_VERSION_URL,
            controller: 'SigninController'
        })

        .state('register', {
            url: '/register',
            title: 'Register',
            templateUrl: 'app/pages/authentication/register.html'+APP_VERSION_URL,
            controller: 'SigninController'
        })

        .state('dashboard', {
            url: '/dashboard',
            title: 'Dashboard',
            templateUrl: 'app/pages/dashboard/dashboard.html'+APP_VERSION_URL,
            controller: 'DashboardController'
        })

        .state('listing', {
            url: '/listing',
            title: 'Listing',
            templateUrl: 'app/pages/listing/listing.html'+APP_VERSION_URL,
            controller: 'ListingController'
        })

        .state('add_listing', {
            url: '/listing/add',
            title: 'Add Listing',
            templateUrl: 'app/pages/listing/add_listing.html'+APP_VERSION_URL,
            controller: 'AddListingController'
        })

        .state('products', {
            url: '/products',
            title: 'Products',
            templateUrl: 'app/pages/products/products.html'+APP_VERSION_URL,
            controller: 'ProductController'
        })

        .state('add_product', {
            url: '/products/add',
            title: 'Add Product',
            templateUrl: 'app/pages/products/add_products.html'+APP_VERSION_URL,
            controller: 'ProductController'
        })

        .state('edit_product', {
            url: '/products/:product_id/edit/',
            title: 'Edit Product',
            templateUrl: 'app/pages/products/add_products.html'+APP_VERSION_URL,
            controller: 'ProductController',
            params : {
                product_id : '',
            }
        })

        .state('research', {
            url: '/research',
            title: 'Research Module',
            templateUrl: 'app/pages/research/research.html'+APP_VERSION_URL,
            controller: 'ResearchController'
        })

        .state('add_research', {
            url: '/research/add-research',
            title: 'Add New Research Idea',
            templateUrl: 'app/pages/research/add_research.html'+APP_VERSION_URL,
            controller: 'ResearchController'
        })

        .state('edit_research', {
            url: '/research/:research_id/edit/',
            title: 'Edit Research Idea',
            templateUrl: 'app/pages/research/add_research.html'+APP_VERSION_URL,
            controller: 'ResearchController',
            params : {
                research_id : '',
            }
        })

        .state('sales_channel', {
            url: '/sales_channel',
            title: 'Sales Channel',
            templateUrl: 'app/pages/sales_channel/sales_channel.html'+APP_VERSION_URL,
            controller: 'SalesChannelController'
        })

        .state('add_sales_channel', {
            url: '/sales_channel/add',
            title: 'Add Sales Channel',
            templateUrl: 'app/pages/sales_channel/add_sales_channel.html'+APP_VERSION_URL,
            controller: 'SalesChannelController'
        })
        
        .state('edit_sales_channel', {
            url: '/sales_channel/:sales_channel_id/edit',
            title: 'Update Sales Channel',
            templateUrl: 'app/pages/sales_channel/add_sales_channel.html'+APP_VERSION_URL,
            controller: 'SalesChannelController'
        })

        .state('edit_sales_product', {
            url: '/sales_channel/product/:sales_channel_product_id/edit',
            title: 'Edit Sales Product',
            templateUrl: 'app/pages/sales_channel/edit_product.html'+APP_VERSION_URL,
            controller: 'SalesChannelController',
            params : {
                sales_channel_product_id : '',
            }
        })

        

        .state('team', {
            url: '/team',
            title: 'Team',
            templateUrl: 'app/pages/team/team.html'+APP_VERSION_URL,
            controller: 'TeamController'
        })

        .state('add_team', {
            url: '/team/add',
            title: 'Add Team',
            templateUrl: 'app/pages/team/add_team.html'+APP_VERSION_URL,
            controller: 'TeamController'
        })

        .state('edit_team', {
            url: '/team/:user_id/edit',
            title: 'Edit Team',
            templateUrl: 'app/pages/team/add_team.html'+APP_VERSION_URL,
            controller: 'TeamController',
            params : {
                user_id : '',
            }
        })


        .state('plans', {
            url: '/plans',
            title: 'Plans',
            templateUrl: 'app/pages/plans/plans.html'+APP_VERSION_URL,
            controller: 'PlansController'
        })

});

app.run(['editableOptions', 'editableThemes', function(editableOptions, editableThemes) {
  editableThemes.bs3.inputClass = 'input-sm';
  editableThemes.bs3.buttonsClass = 'btn-sm';
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
}]);

app.run(function ($state , $rootScope, $location, $timeout, $api, AunthenticationService, localStorageService ,$window) {    
    $rootScope.$on("$locationChangeSuccess", function (event, next, current) {
        //ask the service to check if the user is in fact logged in

        if($state.current.name != "signin" && $state.current.name != "register" && $state.current.name != "register_type") {
            var token = localStorageService.get("token")
            var url = "/user/check/login?token="+token
            $api.get(url).then(function(response) {
                if(response.status == 200) {
                    if(response.data.status == true) {
                        AunthenticationService.setAuth(true);
                        localStorageService.set("credentials", response.data.credentials);
                        localStorageService.set("profile", response.data.profile);
                        localStorageService.set("token", response.data.token);
                    }else if(response.data.status == false) {
                        AunthenticationService.setAuth(false);
                        AunthenticationService.setData({});
                        localStorageService.remove("credentials");
                        localStorageService.remove("is_lock");
                        localStorageService.remove("token");
                    }
                }

            });
        }else{

            if(localStorageService.get("credentials") == null) {
               AunthenticationService.logout();
            }
        }

    });
});



app.filter('capfirst', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});


app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];
    console.log(props)

    if (angular.isArray(items)) {
      var keys = Object.keys(props);
      console.log(keys)
      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

app.filter('filterSubuser', function() {
  return function(items, props) {
    var out = [];
    if (angular.isArray(items)) {
      var keys = Object.keys(props);
      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item["subuser"]["email"].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});




app.run(['$rootScope', '$state',function($rootScope, $state){
  $rootScope.$on('$stateChangeStart',function(){
      $rootScope.stateIsLoading = true;
 });


}]);

app.run(function($window, $rootScope) {
      $rootScope.online = navigator.onLine;
      $window.addEventListener("offline", function() {
        $rootScope.$apply(function() {
          $rootScope.online = false;
        });
      }, false);

      $window.addEventListener("online", function() {
        $rootScope.$apply(function() {
          $rootScope.online = true;
        });
      }, false);
});



//after


app.filter('stringJobType',
[ '$filter', '$locale', function(filter, locale) {

    var currencyFilter = filter('currency');
    var formats = locale.NUMBER_FORMATS;

    return function(param, currencySymbol) {    

      
        if(param==1){
            return 'Anesthesiologist';
        }else{
            return 'CRNA';  
        }
    }  
 
} ]);

app.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
         var doc = new DOMParser().parseFromString(text, "text/html");
         var   rval= doc.documentElement.textContent;
        return $sce.trustAsHtml(rval)
    };
}]);