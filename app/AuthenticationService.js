
	   
app.service('AunthenticationService', function($http, $q, localStorageService) {

	var pipe = {};
	var auth = false;
	var data = {};
	

	pipe.get_crendentials = function(data) {
		var deferred = $q.defer();
    	var postData = pipe.arrayToParams(data);
		$http({
	        method : "POST",
	        url : API_URL+"/credentials",
	        data: postData
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });

	    return deferred.promise;
	}

	pipe.login = function(data) {
		var deferred = $q.defer();
    	var postData = pipe.arrayToParams(data);
		$http({
	        method : "POST",
	        url : API_URL+"/login",
	        data: postData
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	deferred.resolve(response);
	    });

	    return deferred.promise;
	}
	pipe.register = function(data) {
		var deferred = $q.defer();
    	var postData = pipe.arrayToParams(data);
		$http({
	        method : "POST",
	        url : API_URL+"/authentication/register",
	        data: postData
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	deferred.resolve(response);
	    });

	    return deferred.promise;
	}

	pipe.setAuth = function(status) {
		auth = status;
	}
	
	pipe.logout = function() {
		var deferred = $q.defer();
		$http({
	        method : "POST",
	        url : API_URL+"/logout?token="+localStorageService.get("token")
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });
	    return deferred.promise;
	}
	pipe.setData = function(data) {
		data = data;
	}
	pipe.isAuthenticated = function() {
		return (localStorageService.get("token") != null && localStorageService.get("token") != undefined && localStorageService.get("token") != "");
	}
	pipe.serverAuthenticated = function() {
		var deferred = $q.defer();
		$http({
	        method : "GET",
	        url : "/checkAuth"
	    }).then(function mySuccess(response) {
	        deferred.resolve(response);
	    }, function myError(response) {
	    	console.log(response)
	    });
	    return deferred.promise;
	}
	pipe.arrayToParams = function($array) {
		var postParam = {};
		for (key in $array) {
	        postParam[key] = $array[key]
	    }
		return postParam;
	}
	return pipe;
}); 


