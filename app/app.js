var app = angular.module('app', ['ngSanitize','ui.router','LocalStorageModule','xeditable']);

var base_url = "/";
var APP_VERSION_URL = "?VERSION=1.0.2";
var BYPASS = false;

app.config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('anesthesia');
});

app.run(['$rootScope', '$state', '$stateParams',
  function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}])
