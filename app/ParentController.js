
app.controller('ParentController', function($q, $scope,$api, $timeout,$rootScope,localStorageService, AunthenticationService,$state,TokenService, $location, $window) {
    
    $scope.settings = {}
    $scope.core = {};
    $scope.core.is_lock = localStorageService.get("is_lock") ? localStorageService.get("is_lock") : false;
	$scope.header_title = "";
	$scope.header_button_func = "";
	$scope.header_button_label = "";
	$scope.authenticated = false;
    $scope.lockminutes = 600000;
	$scope.token;
	$scope.remove = [];
	$scope.loadingPage = true;
    $scope.newMessage = 0;
    $scope.recentConversations = [];
    $scope.notifications = [];
    $scope.current_index = 0;
    $scope.notif_counter = 0;
    $scope.basicInfo = {};
    $scope.socialInfo = {};
    $scope.extraInfo = {};
    $scope.jobpost = {};
    $scope.filters= {};
    $scope.timeout= {};
    $scope.timeout_status = null
    $scope.current_user_status = "online";
    $scope.timeout = {};
    $scope.jobpost.quantity = 1;
    $scope.jobpost.type = 1;   
    //$scope.user_is_online = false; 


    $scope.removeCreds = function() {
        AunthenticationService.setAuth(false);
        AunthenticationService.setData({});
        localStorageService.remove("credentials");
        localStorageService.remove("is_lock");
    }

    //$scope.removeCreds();
    $scope.isLogin = function() {
        return AunthenticationService.isAuthenticated();
    }
    $scope.removeHtml = function(string){
        return string.replace(/(<([^>]+)>)/ig,"");
    }

    $scope.showModalMethod = function(){
        var inviteDevInput = '<form> <div class="">'+
                            '<input type="radio" class="with-gap radio-col-purple" name="csvSelect" id="radio1" ng-model="jobpost.quantity" ng-value="1" value="1" >'+
                            '<label for="radio1"> Post a single job </label>'+
                            '</div> <div class="">'+
                            '<input type="radio" class="with-gap radio-col-purple" name="csvSelect" id="radio2" ng-model="jobpost.quantity" ng-value="2" ng-click="getVal()" value="2">'+
                            '<label for="radio2"> Upload from .csv file </label> </div></form>'+
                            '<br/>'+
                            '<a href="/downloads/job_template" target="_blank">Download Job template</a>'+        
                            '<br/>'+
                            '<a href="/downloads/tips" target="_blank">Download Helpful tips</a>';
                            
                            
        swal({
                title: "Choose method",
                text: inviteDevInput,
                html: true,
                showCancelButton: true,
                confirmButtonColor: "#006DCC",
                confirmButtonText: "Proceed",
                cancelButtonText: "Close",
                closeOnConfirm: false,
                closeOnCancel: true
            }, // Function that validates email address through a regular expression.
            function () {                
                recordType = $("input[name=csvSelect]:checked").val();
                if(recordType == 1){
                    $state.go('new_job',{profession : $scope.jobpost.type})
                    angular.element('#jobPostOptionModal').modal('hide');
                    $scope.jobpost = {};
                    $scope.jobpost.quantity = "";
                    $scope.jobpost.type = "";
                    swal.close();
                }else if(recordType == 2){
                    angular.element('#jobPostOptionModal').modal('hide');            
                    $state.go('jobs_list',{showSpreedsheet : true})
                    $scope.jobpost.quantity = "";
                    $scope.jobpost.type = "";
                    angular.element('#uploadSpreadsheet').modal('show')
                    $scope.jobpost.quantity = 1;
                    swal.close();
                }else{                    
                    swal.showInputError("Please select method first")
                }

            }
        );
    }
    $scope.proceedToNewjob = function(){        

        if($scope.jobpost.quantity == 1){// && $state.current.name != 'new_job'
            $state.go('new_job',{profession : $scope.jobpost.type})
            angular.element('#jobPostOptionModal').modal('hide');
            $scope.jobpost = {};
            $scope.jobpost.quantity = "";
            $scope.jobpost.type = "";
        }else{
            angular.element('#jobPostOptionModal').modal('hide');            
            $state.go('jobs_list',{showSpreedsheet : true})
            $scope.jobpost.quantity = "";
            $scope.jobpost.type = "";
            angular.element('#uploadSpreadsheet').modal('show')
            $scope.jobpost.quantity = 1;                    
        }
        
    }

    $scope.remBillingActiveState = function(){
        angular.element('#billing').removeClass('active')
    }

    $scope.getSubUser = function(){
        if(localStorageService.get("subuser") == null){
            return 0;
        }else{
            return 1;
        }
    }        

    $scope.storage = function(key) {
        if(localStorageService.get(key) != undefined) {
           return localStorageService.get(key); 
        }

        return "";
    }

    $scope.profiletitle = function(){
        if(localStorageService.get("credentials").userType == 2){
            return localStorageService.get("profile").company_name;
        }else{
            return localStorageService.get("profile").firstName + ' ' + localStorageService.get("profile").lastName;
        }
    }

    $scope.testcall = function(){                
        var toogler = $('#nav-toggler').hasClass('ti-menu')        
        if(toogler == true)
        {
            $('#nav-toggler').addClass('ti-menu')
        }else{
            $('#nav-toggler').removeClass('ti-menu')
        }
    }

    $scope.pushNewData = function(data) {
        angular.forEach(data, function(notif) {
            $scope.notifications.push(notif)
        });
    }
    $scope.$watch('online', function(newStatus) { 
        if(newStatus == false && 1 == 2) {
            swal({   
                title: "Connecting...",   
                text: "Please check your internet connection.",   
                showConfirmButton: false 
            });
        }else{
            swal.close();
        }
    });


    $scope.parentTriggger = function() {
        
        $timeout(function() {
            $(window).trigger('resize');
            // console.log("resizing triggered")
        },1200);
        
    }

     $scope.getAuth = function() {
        var data = localStorageService.get("credentials");  

        if(data != null) 
            return data;

        return null;
    }

    
    $scope.onlineStatus = function() {
        $scope.Echo.private('onlineStatus.'+$scope.getUser('id'))
            .listen('OnlineStatus',function(e){
        });
    }

    $scope.listenNewMessages = function() {
        $scope.Echo.private('newMessage.'+$scope.getUser('id'))
            .listen('NotifyNewMessageUser',function(e){
                counter = 0;
                $scope.audioNewMessage();
                angular.forEach($scope.recentConversations, function(convo, key){
                    // console.log(convo)
                    if(convo.convo_id == e.data.conversation.id) {
                        counter++;
                        $scope.recentConversations[key] = e.recentMessages;
                    }
                });

                if(counter == 0) {
                    $scope.recentConversations.push(e.recentMessages);
                }

                
                $scope.newMessage = $scope.newMessage + 1;
                $scope.$apply();
            });
    }

    $scope.getNotifications = function() {
        var url = '/mailbox/list'
        $api.get(url).then(function(response){
            $scope.notifications = response.data        
        });
    }

    $scope.showMail = function(mail){
        $state.go('mailbox',{notif_mail : mail});
    }

    $scope.listenNotifications = function() {
        $scope.Echo.private('notifications.'+$scope.getUser('id'))
            .listen('Notifications',function(e){
                // console.log(e)
                $scope.notif_counter = 1;
                $scope.audioNewMessage();
                
             /*   angular.forEach($scope.notifications, function(convo, key){
                    console.log(convo)
                     $scope.notifications[key] = e.data;
                });*/
                if($scope.notif_counter == 0) {
                    $scope.notifications = [];
                    $scope.notifications[0] = e.data;
                    $scope.notif_counter++;
                }else{
                    $scope.notifications.unshift(e.data);
                }
                
                $scope.$apply();
            });
    }

	/*TokenService.get().then(function(response) {
		$scope.token = response;

        $('meta[name=csrf-token]').attr('content', response.data);
    
	});
*/
	$scope.goto = function(location) {
		console.log(location)
		$state.go(location);
	}
	$scope.callFunction = function (fname) {
        if(angular.isFunction($scope[fname]))
           $scope[fname]();
    }

    $scope.isAuthenticated = function() {
    	return $scope.authenticated;
    }
    $scope.logout = function() {

    	//notif_warning("Logout", "Logging out.")
    	AunthenticationService.logout().then(function(response) {
    		if(response.data.status) {
    			AunthenticationService.setAuth(false);
				AunthenticationService.setData({});
				localStorageService.remove("credentials");
                localStorageService.remove("token");
                localStorageService.remove("is_lock");
				$scope.authenticated = false;//parent controller
                $(".loading-base").removeClass("hidden");
                $timeout(function() {
                        $window.location.href = '/#/signin';
                },1500);
                
    		}
/*
            TokenService.get().then(function(response) {
                $scope.token = response;
            });*/
                
    	});
    }
    $scope.getUserProfile = function(key) {
        var data = localStorageService.get("profile");        
        if(data != null) 
            return data[key];
    }

    $scope.stopImpersonate = function() {
        var url = "/users/login_as/stop"
        $api.get(url).then(function(response) {
            $scope.getUserProfileDb();
            $state.go("manage_accounts");
        });
    }

    $scope.parseUrl = function(path) {
        return path;
    }

    $scope.updateSelectedIndex = function(id) {
        $scope.selected_index = id;
        $scope.$apply();
    }
    
    $scope.getUser = function(key) {
    	var data = localStorageService.get("credentials");  

    	if(data != null) 
    		return data[key];
    }
    
	$scope.getUserProfileDb = function() {
        var url = "/user/getProfile"
        $api.get(url).then(function(response) {
            $scope.basicInfo = response.data;
            $scope.socialInfo = response.data;
            $scope.extraInfo = response.data;
            // console.log($scope.basicInfo)
            $scope.initialization = true;
        });
    }

	$scope.isAdminUser = function() {
    	var data = localStorageService.get("credentials");
		var userAdmin = data.userAdmin;
		 return false;
    }

    $scope.unlockApp = function() {
        var url = "/lockscreen/unlock";
        var data = $scope.lockscreen;

        $api.post(data, url).then(function(response) {
            if(response.data.status) {
                $scope.lockscreen = {}
                localStorageService.set("is_lock", !response.data.status);
                $scope.core.is_lock = !response.data.status;
                $scope.current_user_status = "online";
            }else{
                swal("Sorry.","Password is incorrect.","info")
            }
            $scope.activeButton.stop();
        });
    }

    $scope.lockNoActivity = function() {
        if($scope.core.is_lock == true && $state.current.name != "" && $state.current.name != "login" && $state.current.name != "register" && $state.current.name != "register_type") {
            var url = "/user/check/login"
            $api.get(url).then(function(response) {
                if(response.data.status == false) {
                    AunthenticationService.setAuth(false);
                    AunthenticationService.setData({});
                    localStorageService.remove("credentials");
                    localStorageService.remove("is_lock");
                    $window.location.href = '/'; 
                }
            });
        }
    }

    $scope.check_status = function() {
        setInterval(function() {
            if(localStorageService.get("credentials").impersonate != true &&  $state.current.name != "" && $state.current.name != "login" && $state.current.name != "register" && $state.current.name != "register_type") {
                var url = "/user/check/login?status="+$scope.current_user_status+"&user_id="+$scope.getUser('id')
                console.log("check_status")
                //$api.get(url);
                $api.get(url).then(function(response){
                    if(response.data.status){
                        //$scope.user_is_online = true;                        
                    }
                });
            }
        }, 10000);//3000 for 3 secs if test / or 180000 for 3 mins 600000

    }


    $scope.noActivity = function() {
        var timeout = null;
        $(document).on('mousemove', function() {
            

            clearTimeout($scope.timeout);

            $scope.timeout = setTimeout(function() {
                console.log("noActivity")
                if(localStorageService.get("credentials").impersonate != true &&  $state.current.name != "" && $state.current.name != "login" && $state.current.name != "register" && $state.current.name != "register_type") {
                    $scope.current_user_status = "away";
                    var url = "/user/check/login?status="+$scope.current_user_status+"&user_id="+$scope.getUser('id')
                    $api.get(url).then(function(response) {
                        if(response.data.status == false) {
                            AunthenticationService.setAuth(false);
                            AunthenticationService.setData({});
                            localStorageService.remove("credentials");
                            localStorageService.remove("is_lock");
                            $window.location.href = '/'; 
                        }else{
                            $scope.core.is_lock = true;
                            localStorageService.set("is_lock", $scope.core.is_lock);
                        }
                    });
                }

            }, $scope.lockminutes);//3000 for 3 secs if test / or 180000 for 3 mins 600000
        });
    }

    $scope.getConversations = function() {
        var url = "/chat/getConversations";

        $api.get(url).then(function(response) {
            $scope.recentConversations = response.data;
            // console.log($scope.recentConversations)
        });
    }

    $scope.audioNewMessage = function() {
        var audio = new Audio('/application/assets/music/new_message.mp3');
        audio.play();
    };

    $scope.getSubUserPermissions = function(){
        var url = "/sub_users/permissions";
        $api.get(url).then(function(response) {                         
            $scope.filters.permissions = response.data;
        });
    }
    
    $scope.hasAccess = function(permission) {
        if($scope.filters.permissions == undefined || $scope.filters.permissions == "") {
            return true;
        }
        return $scope.filters.permissions[permission];
    }

    $scope.reload_custom = function() {
        $timeout(function() {
            $('<script>').attr('src', "/assets/js/perfect-scroll.js").appendTo('head');
            $('<script>').attr('src', "/assets/js/material-dashboard.min.js").appendTo('head');
        })
        
    }
    $scope.$on("$stateChangeStart", function(event, next, current) {
        if($state.current.name == "add_research") {
            if (!confirm("Are you sure you want to close this?, you will lose unsave changes.")) {
                event.preventDefault();
            }
        }    
    });
    $scope.reload_custom();

});

